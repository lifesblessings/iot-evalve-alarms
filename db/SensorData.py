from mongoengine import *

class Temperature(EmbeddedDocument):
    temp    = FloatField(required=True)

class UnknownData(EmbeddedDocument):
    rawData = StringField(required=True)

class SensorData(Document):
    sensorId    = StringField(required=True)
    beamerId    = StringField(required=True)
    data        = GenericEmbeddedDocumentField()
    rssi        = IntField(required=True)
    timestamp   = DateTimeField(required=True)
    dataType    = StringField(required=True)
    meta = {
        'indexes' : [
            'timestamp',
            '$dataType'
        ]
    }

class AngleData(EmbeddedDocument):
    refId = StringField(required=True)
    refrssi = IntField(required=True)
    refx = FloatField()
    refy = FloatField()
    sensx = FloatField()
    sensy = FloatField()

