from mongoengine import *

class Alarm(Document):
    name          = StringField(required=True,unique=True)
    createdAt     = DateTimeField(required=True)
    updatedAt     = DateTimeField(required=True)
    refValue      = StringField(required=True)#reference to compare value with
    refValueBet   = StringField()#optional second reference to compare value with
    refCondition  = StringField(required=True)#possible operand for comparation
    dataType      = StringField()#float and int for now
    interval      = IntField(required=True)#interval in seconds to evaluate condition (period) 
    refCollection = StringField(required=True) #reference collection to extract data to compare
    refData       = StringField(required=True) #value to compare
    mailTo        = StringField(required=True)
