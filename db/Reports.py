from mongoengine import ( Document, EmbeddedDocument, IntField, DateTimeField, StringField, FloatField, ListField )

class BatteryReport(EmbeddedDocument):
    percent = IntField(required=True)
    ts      = DateTimeField(required=True)

class PositionReport(Document):
    sensorId = StringField(required=True)
    referenceId = StringField(required=True)
    ts = DateTimeField(required=True)
    created = DateTimeField(required=True)
    angle = FloatField(required=True)
    tag = StringField()

class ReportList(Document):
    name        = StringField(required=True)
    description = StringField(required=True)
    createdAt   = DateTimeField(required=True)
    owner       = StringField(required=True)
    formula     = StringField()
    data        = ListField(required=True)