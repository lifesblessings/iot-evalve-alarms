from mongoengine import *
from db.SensorData import *
from db.Alarms import *
from db.Reports import *
from smtp import sendemail
import time as t
from datetime import datetime as dt
from datetime import timedelta as td
from JsonManager import *
import json
import os


def check(interval,alarm_name):
    alarm = Alarm.objects.get(name=alarm_name)
    if((str(alarm.refCondition)) == "Hallado" or (str(alarm.refCondition)) == "No Hallado"):
        any_alarm(alarm)
    elif(str(alarm.dataType) == "Fecha"):
        date_alarm(alarm)
    else:
        num_alarm(alarm)

def any_alarm(alarm):
    rep = eval(str(alarm.refCollection))
    trigger = None
    records = rep.objects(ts__gte=dt.now()-td(seconds=float(alarm.interval)))
    if(str(alarm.refCondition))=="Hallado" and records:
        for record in records:
            if(record[str(alarm.refData)]):
                trigger = True
                break

    elif(str(alarm.refCondition))=="No Hallado" and not records:
        print("no Hallado")
        # sendemail('soporte@innovaxxion.com',[str(alarm.mailTo)],[],'Innovaxxion','\nAlarma\n','soporte@innovaxxion.com','innova2k+1')
    elif (trigger is None) and ((str(alarm.refCondition))=="No Hallado") and records:
        print("no Hallado")
        # sendemail('soporte@innovaxxion.com',[str(alarm.mailTo)],[],'Innovaxxion','\nAlarma\n','soporte@innovaxxion.com','innova2k+1')    

    if trigger:
        print("Hallado")
        # sendemail('soporte@innovaxxion.com',[str(alarm.mailTo)],[],'Innovaxxion','\nAlarma\n','soporte@innovaxxion.com','innova2k+1')


def date_alarm(alarm):
    rep = eval(str(alarm.refCollection))
    #records = rep.objects(ts__gte=dt.now()-td(seconds=interval))
    records = rep.objects[:5]
    if alarm.refValueBet and (str(alarm.refCondition) == "Entre" or str(alarm.refCondition) == "No Entre"):
        if str(alarm.refCondition) == "Entre":
            for record in records:
                if dt.strptime(alarm.refValue, r'%Y-%m-%d %H:%M:%S')>=record[str(alarm.refData)]<=dt.strptime(alarm.refValueBet, r'%Y-%m-%d %H:%M:%S'):
                    print("entre")
                    # sendemail('soporte@innovaxxion.com',[str(alarm.mailTo)],[],'Innovaxxion','\nAlarma\n','soporte@innovaxxion.com','innova2k+1')
        else:
            for record in records:
                if dt.strptime(alarm.refValue, r'%Y-%m-%d %H:%M:%S')<=record[str(alarm.refData)] or record[str(alarm.refData)]>=dt.strptime(alarm.refValueBet, r'%Y-%m-%d %H:%M:%S'):
                    print("no entre")
                    # sendemail('soporte@innovaxxion.com',[str(alarm.mailTo)],[],'Innovaxxion','\nAlarma\n','soporte@innovaxxion.com','innova2k+1')
    elif str(alarm.refCondition) == "Mayor":
        for record in records:
            if dt.strptime(alarm.refValue, r'%Y-%m-%d %H:%M:%S')<=record[str(alarm.refData)]:
                print("mayor")
                # sendemail('soporte@innovaxxion.com',[str(alarm.mailTo)],[],'Innovaxxion','\nAlarma\n','soporte@innovaxxion.com','innova2k+1')
    elif str(alarm.refCondition) == "Menor":
        for record in records:
            if dt.strptime(alarm.refValue, r'%Y-%m-%d %H:%M:%S')>record[str(alarm.refData)]:
                print("menor")
                # sendemail('soporte@innovaxxion.com',[str(alarm.mailTo)],[],'Innovaxxion','\nAlarma\n','soporte@innovaxxion.com','innova2k+1')
    elif str(alarm.refCondition) == "Mayor o Igual":
        for record in records:
            if dt.strptime(alarm.refValue, r'%Y-%m-%d %H:%M:%S')<=record[str(alarm.refData)]:
                print("mayor igual")
                # sendemail('soporte@innovaxxion.com',[str(alarm.mailTo)],[],'Innovaxxion','\nAlarma\n','soporte@innovaxxion.com','innova2k+1')
    elif str(alarm.refCondition) == "Menor o Igual":
        for record in records:
            if dt.strptime(alarm.refValue, r'%Y-%m-%d %H:%M:%S')>=record[str(alarm.refData)]:
                print("menor o igual")
                # sendemail('soporte@innovaxxion.com',[str(alarm.mailTo)],[],'Innovaxxion','\nAlarma\n','soporte@innovaxxion.com','innova2k+1')
    elif str(alarm.refCondition) == "Igual":
        for record in records:
            if dt.strptime(alarm.refValue, r'%Y-%m-%d %H:%M:%S')==record[str(alarm.refData)]:
                print("igual")
                # sendemail('soporte@innovaxxion.com',[str(alarm.mailTo)],[],'Innovaxxion','\nAlarma\n','soporte@innovaxxion.com','innova2k+1')
    elif str(alarm.refCondition) == "Distinto":
        for record in records:
            if dt.strptime(alarm.refValue, r'%Y-%m-%d %H:%M:%S')!=record[str(alarm.refData)]:
                print("distinto")
                # sendemail('soporte@innovaxxion.com',[str(alarm.mailTo)],[],'Innovaxxion','\nAlarma\n','soporte@innovaxxion.com','innova2k+1')

def num_alarm(alarm):
    rep = eval(str(alarm.refCollection))
    #records = rep.objects(ts__gte=dt.now()-td(seconds=interval))
    records = rep.objects[:5]
    if alarm.refValueBet and (str(alarm.refCondition) == "Entre" or str(alarm.refCondition) == "No Entre"):
        if str(alarm.refCondition) == "Entre":
            for record in records:
                if float(alarm.refValue)>=record[str(alarm.refData)]<=float(alarm.refValueBet):
                    print("entre")
                    # sendemail('soporte@innovaxxion.com',[str(alarm.mailTo)],[],'Innovaxxion','\nAlarma\n','soporte@innovaxxion.com','innova2k+1')
        else:
            for record in records:
                if float(alarm.refValue)<=record[str(alarm.refData)] or record[str(alarm.refData)]>=float(alarm.refValueBet):
                    print("no entre")
                    # sendemail('soporte@innovaxxion.com',[str(alarm.mailTo)],[],'Innovaxxion','\nAlarma\n','soporte@innovaxxion.com','innova2k+1')
    elif str(alarm.refCondition) == "Mayor":
        for record in records:
            if float(alarm.refValue)<=record[str(alarm.refData)]:
                print("mayor")
                # sendemail('soporte@innovaxxion.com',[str(alarm.mailTo)],[],'Innovaxxion','\nAlarma\n','soporte@innovaxxion.com','innova2k+1')
    elif str(alarm.refCondition) == "Menor":
        for record in records:
            if float(alarm.refValue)>record[str(alarm.refData)]:
                print("menor")
                # sendemail('soporte@innovaxxion.com',[str(alarm.mailTo)],[],'Innovaxxion','\nAlarma\n','soporte@innovaxxion.com','innova2k+1')
    elif str(alarm.refCondition) == "Mayor o Igual":
        for record in records:
            if float(alarm.refValue)<=record[str(alarm.refData)]:
                print("mayor igual")
                # sendemail('soporte@innovaxxion.com',[str(alarm.mailTo)],[],'Innovaxxion','\nAlarma\n','soporte@innovaxxion.com','innova2k+1')
    elif str(alarm.refCondition) == "Menor o Igual":
        for record in records:
            if float(alarm.refValue)>=record[str(alarm.refData)]:
                print("menor o igual")
                # sendemail('soporte@innovaxxion.com',[str(alarm.mailTo)],[],'Innovaxxion','\nAlarma\n','soporte@innovaxxion.com','innova2k+1')
    elif str(alarm.refCondition) == "Igual":
        for record in records:
            if float(alarm.refValue)==record[str(alarm.refData)]:
                print("igual")
                # sendemail('soporte@innovaxxion.com',[str(alarm.mailTo)],[],'Innovaxxion','\nAlarma\n','soporte@innovaxxion.com','innova2k+1')
    elif str(alarm.refCondition) == "Distinto":
        for record in records:
            if float(alarm.refValue)!=record[str(alarm.refData)]:
                print("distinto")
                # sendemail('soporte@innovaxxion.com',[str(alarm.mailTo)],[],'Innovaxxion','\nAlarma\n','soporte@innovaxxion.com','innova2k+1')


def main():
    cm = JsonManager(os.path.join(os.path.dirname(__file__), "config_params.json"))
    connect(cm.data['database']['name'], host='mongodb://'+cm.data['database']['ip'], port=cm.data['database']['port'])
    alarms = Alarm.objects()
    times = []
    for alarm in alarms:
        times.append({"interval":alarm.interval,"checked":dt.now(),"name":alarm.name})
    while True:
        for time in times:
            #if(dt.now()-time["checked"]>td(seconds=time["interval"])):
            if(True):
                time["checked"]=dt.now()
                check(time["interval"],time["name"])
                t.sleep(10)#testing
                
if __name__ == "__main__":
    main()